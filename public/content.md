![AAIT Logo](http://www.exuberantsolutions.com/images/LOGOS/Addis_Ababa_University_logo.png)
# Addis Ababa University
## Addis Ababa Institute of Technology
## Center of Information Technology and Scientific Computing
## Software Engineering
## Fundamentals of Information Security
## Lab 1 Report
## Submitted to: Mr. Abebe
## Henok Million (ATR/1810/08)
## December, 2018

# AES (Advanced Encryption Standard) a.k.a Rijndael
- 128 bit blocks
- same key for encrypting and decrypting
- **IV**
	- input for iteration
	- different output on each encryption iteration

for the experiments below, the plain text is 
_plain.txt_
```
android
bin
Customization
Desktop
Documents
Downloads
examples.desktop
host
lib
Music
Pictures
plain.txt
Public
source
Templates
Videos
```

# Blowfish
-  general-purpose algorithm intended to replace DES and solve problems of other algorithms
- 64-bit block size
- variable key, of length 32-448 bits
- **operations**:
	- XOR left half with r'th P-array entry (18-entry array)
	- use the XORed data as input for Blowfish's F-function
	- XOR the function's output with the right half
	- swap the left and the right halves
![Blowfish mode encryption](https://upload.wikimedia.org/wikipedia/commons/thumb/5/5e/Blowfish_diagram.png/320px-Blowfish_diagram.png)
```
$ openssl enc -bf-cbc -e -in plain.txt -out cipher-bf-cbc.bin -K 00112233445566778899aabbccddeeff -iv 0102030405060708
```
yielded in:
![bf-cbc](./bf-cbc.png)
- due to it's small block size [64-bits], it could not be used to encrypt files larger than 4 GB.
- the small block size also makes it succeptible to birthday attacks
	- birthday attacks exploit the probability that two outputs be the same and from that, trying to get the key, and after, the plain text; a neat exploit based on mathematical probability

# CBC (Cipher Block Chaining)
- CBC mode, each block of plaintext is XORed with the previous ciphertext block before being encrypted.
- uses IV (Initialization vector) to XOR previous cipertext bloks before encryption. The IV would be used in the first
- the next block would use the IV ouput of the first block's encryption as it's IV, hence patterns are not easily identified and creates an inter-dependent blocks that would be, individually hard to decrypt
![CBC mode encryption - _wikipedia_](https://upload.wikimedia.org/wikipedia/commons/thumb/2/2a/CBC_decryption.svg/601px-CBC_decryption.svg.png)

```
$ openssl enc -aes-128-cbc -e -in plain.txt -out cipher-aes128cbc.bin -K 00112233445566778899aabbccddeeff -iv 0102030405060708
```
- an aes, 128-block-sized cbc encryption method yielded in:
![aes-128-cbc](./aes128cbc.png)
- a 144-byte-long cipher

# CFB (Cipher Feedback)
- IV is first encypted with the key, then XORed with the plaintext, 
- the output ciphertext would be used as an IV for the consequential blocks
![CFB mode encryption](https://upload.wikimedia.org/wikipedia/commons/thumb/9/9d/CFB_encryption.svg/601px-CFB_encryption.svg.png)
```
openssl enc -aes-128-cbc -e -in plain.txt -out cipher-aes128cbc.bin -K 00112233445566778899aabbccddeeff -iv 0102030405060708
```
yielded an 136-bytes-long cipher
![CFB encryption output](./aes-128-cfb.png)
**Note**: this cipher is shorter than the previous one, done with cbc. My hypothesis is this occurs has to do with the seqence in which the plaintext and the IV are XORed. Here, the plaintext is XORed with the output of the encryption function which already used up the IV and the key, hence the final output, of the XOR operation, would be relatively smaller than the one in CBC, which pre-XORs the plaintext and the IV, hence leaving the final output to be the one from the encryption function.
- One more thing to not in the CFB block is the poisoning of bits. Changing bits would destroy the sequence after the block where the alteration occured, and would affect the bits altered in it. This would leave the rest of the block, and part of the current block un-decipherable.


# ECB (Electronic Codebook)
- does not use the initialization vector
- encrypts each block with a key
- using the same key to independently encrypt the plain text blocks is a weakness, as enough cipher blokcs could enable for identification of the encryption pattern
![ECB mode encryption](https://upload.wikimedia.org/wikipedia/commons/thumb/d/d6/ECB_encryption.svg/601px-ECB_encryption.svg.png)
- Does not use an IV.
```
$ openssl enc -aes-128-ecb -e -in plain.txt -out cipher-aes-128-ecb.bin -k 0011223344556677
```
yields: 
![ecb encryption output](./ecb.png)
- since the same key is used to encrypt each block, a pattern can be developed in the output that could make decrypting easier. For example, in encrypting an image, colors always are projected to fixed values, and hence would not be hard to see the original picture. 

# DES (Data Encryption Standard)
- DES uses 64-bit keys
	- in reality, each byte has a parity bit and the key would be 56 bits
	- the output is 64 bit
- half-blocks would be 32-bits, and is expanded to 48 bits using _expansion permutation, E_
- 16 rounds of encryption take place, and for each iteration a sub-key would be yielded
- to decrypt though, the keys would be used in reverse order; like the 16th key for the first round
- on each iteration, a half would be encrypted with the key combined with the other half, and swapped
![DES encryption mode](https://upload.wikimedia.org/wikipedia/commons/thumb/6/6a/DES-main-network.png/375px-DES-main-network.png)
```
openssl enc -des -e -in plain.txt -out cipher-des.bin -k 0011223344556677
```
yields:
![des encryption output](./des.png)










